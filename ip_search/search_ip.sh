#!/bin/bash

echo "Digite o endereço IP que deseja procurar:"
read ip_search

echo "Digite a região que deseja procurar:"
read region_search

echo "Digite o profile que deseja procurar. Se não for especificar algum digite \"default\":"
read profile

echo "Seria o endereço $ip_search, com a região $region_search e profile $profile? Se sim digite (y), se não digite (n)."
read confirm
if [ $confirm = y ]; then

echo "Efetuando comandos, se o endereço IP for encontrado será informado:"
echo "


--------------------------------------------------------"

echo "Procurando em EC2 limitado pela conta, região e profile escolhidos:"
## EC2
aws ec2 describe-instances --query=Reservations[].Instances[].PublicIpAddress --region $region_search --profile $profile | grep -o $ip_search > search_ip_encontrado.txt
search=$(cat search_ip_encontrado.txt)
if [[ "$ip_search" == "$search" ]]; then
	echo "
	---Encontrado!!!---
	"
    cat search_ip_encontrado.txt
	else
	:
	fi
echo "


--------------------------------------------------------"
echo "Procurando no Elastic IP limitado pela conta, região e profile escolhidos:"
## Elastic IP 
aws ec2 describe-addresses --query Addresses[*].PublicIp --region $region_search --profile $profile | grep -o $ip_search > search_ip_encontrado.txt
search=$(cat search_ip_encontrado.txt)
if [[ "$ip_search" == "$search" ]]; then
	echo "
	---Encontrado!!!---
	"
    cat search_ip_encontrado.txt
	else
	:
	fi
echo "


--------------------------------------------------------"
echo "Procurando nomes de Load Balancer limitados pela conta, região e profile escolhidos:"
## Load Balancer
aws elbv2 describe-load-balancers --query LoadBalancers[*].DNSName --region $region_search --profile $profile | sed 's/,//' | sed 's/"//g' | cut -c 5-100 > search_ip_temp.txt

while IFS= read -r linha; do
	nslookup $linha > search_ip_encontrado.txt
	search=$(grep -o $ip_search search_ip_encontrado.txt)
	if [[ "$ip_search" == "$search" ]]; then
	echo "
	---Encontrado!!!---
	"
    grep -B 1 $ip_search search_ip_encontrado.txt
	break
	else
	echo " "
	fi
done < search_ip_temp.txt

echo "

--------------------------------------------------------"
echo "Procurando relação de nomes de RDS limitados pela conta, região e profile escolhidos:"
## RDS
aws rds describe-db-instances --query=DBInstances[*].Endpoint.Address --region $region_search --profile $profile | sed 's/,//' | sed 's/"//g' | cut -c 5-100 > search_ip_temp.txt

while IFS= read -r linha; do
	nslookup $linha > search_ip_encontrado.txt
	search=$(grep -o $ip_search search_ip_encontrado.txt)
	if [[ "$ip_search" == "$search" ]]; then
	echo "
	---Encontrado!!!---
	"
    grep -B 1 $ip_search search_ip_encontrado.txt
	break
	else
	echo " "
	fi
done < search_ip_temp.txt

echo "

--------------------------------------------------------"
echo "Procurando informações do Elastic Beanstalk limitados pela conta e região escolhida:"
## Elastic Beanstalk
aws elasticbeanstalk describe-environments --query Environments[*].EndpointURL --region $region_search --profile $profile | sed 's/,//' | sed 's/"//g' | cut -c 5-100 > search_ip_temp.txt

while IFS= read -r linha; do
	nslookup $linha > search_ip_encontrado.txt
	search=$(grep -o $ip_search search_ip_encontrado.txt)
	if [[ "$ip_search" == "$search" ]]; then
	echo "
	---Encontrado!!!---
	"
    grep -B 1 $ip_search search_ip_encontrado.txt
	break
	else
	echo " "
	fi
done < search_ip_temp.txt

echo "

--------------------------------------------------------"
echo "Procurando informações de API Gateway limitados pela conta e região escolhida:"
## API Gateway
# The DNS name can be constructed from the values of REST API id and the AWS region using the following format: <id>.execute-api.<region>.amazonaws.com
aws apigateway get-rest-apis --region $region_search --profile $profile | grep \"id\": | cut -c 20-29 | sed "s/$/.execute-api.$region_search.amazonaws.com/" > search_ip_temp.txt

while IFS= read -r linha; do
	nslookup $linha > search_ip_encontrado.txt
	search=$(grep -o $ip_search search_ip_encontrado.txt)
	if [[ "$ip_search" == "$search" ]]; then
	echo "
	---Encontrado!!!---
	"
    grep -B 1 $ip_search search_ip_encontrado.txt
	break
	else
	:
	fi
done < search_ip_temp.txt

else
echo "Saindo... Se digitou errado tente novamente!"
exit
fi

echo "


--------------------------------------------------------
Fim! Se nada foi encontrado tente rodar em outra conta... 
"

rm search_ip_temp.txt
rm search_ip_encontrado.txt

exit

